import React from 'react';
import List from './component/List';
import { Switch, Route } from 'react-router-dom';
import Post from './component/Post/Post';



function App() {
  return (
      <Switch>
      <Route exact path="/">
        <List />
      </Route>
      <Route path="/posts/:id">
        <Post />
      </Route>
    </Switch>
  );
}

export default App;
