import {GET_POSTS, GET_POSTS_BY_ID} from '../posts/postActionTypes'
import Axios from 'axios'
export const getPosts = () =>{
    return async dp => {
        const result = await Axios.get('https://jsonplaceholder.typicode.com/posts')
        dp({
          type: GET_POSTS,
          data: result.data
        })
    }
}
export const getPostById = (id) =>{
    return async dp => {
      const result = await Axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      dp({
        type: GET_POSTS_BY_ID,
        data: result.data
      })
    }
}

