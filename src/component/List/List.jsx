import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {getPosts} from '../../redux/actions/posts/postAction'
import {Container} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

const List = (props) => {
  useEffect(() => {
    props.getPosts();
  },[]);
  return(
    <Container className="ListPostWrapper">
      {props.data.map((d) => {
        return (
            <React.Fragment key={d.id}>
              Tutorial: {d.id}
              {d.descrption}
              {" : "}
              <Link to={`/posts/${d.id}`}>{d.title}</Link>
              <br />
            </React.Fragment>
        );
      })}
    </Container>
  )
}

const mapStateToProps = state => {
  return{
    data: state.data
  }
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getPosts
  },dispatch)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(List);
